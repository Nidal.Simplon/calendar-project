# calendar-project

### Description

Version 1.0.0

Cette application de calendrier vous permet d'ajouter et de modifier des événements tout au long de la journée.

------

### **L'image du calendrier**

![Ordinateur](https://gitlab.com/Nidal.Simplon/calendar-project/raw/master/Images/Ordinateur.png)

------

### Application responsive

Notre application est compatible aux écrans d'ordinateurs, de tablettes et de portables.

##### Pour tablettes

![Tablete](https://gitlab.com/Nidal.Simplon/calendar-project/raw/master/Images/Tablete.png)

##### Pour portables

![Portable](https://gitlab.com/Nidal.Simplon/calendar-project/raw/master/Images/Portable.png)

----

### Développer avec : 

- HTML
- CSS
- JavaScript
- Webpack
- Moment.js